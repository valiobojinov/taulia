package com.taulia.converter.processor.outbound;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.taulia.converter.entity.InvoiceEntity;
import com.taulia.converter.services.correlation.PayloadCorrelationService;

public class CsvOutboundProcessor implements OutboundProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(CsvOutboundProcessor.class);
	private final String baseLocation;
	private final CsvMapper mapper;
	private final PayloadCorrelationService correlationService;

	
	public CsvOutboundProcessor(String baseLocation, CsvMapper mapper, PayloadCorrelationService correlationService) {
		this.baseLocation = baseLocation;
		this.mapper = mapper;
		this.correlationService = correlationService;
	}
	public int process(String customer, List<InvoiceEntity> entities) {
		int size = entities.size();
		try {
			String suffix = UUID.randomUUID().toString();
			
	        CsvSchema schema = mapper.schemaFor(InvoiceEntity.class).withUseHeader(true).withColumnSeparator(',').withoutQuoteChar();
	        //now let's wrap some streams ;)
	        File targetFile = new File(baseOututLocation(), customer + "-" + suffix + "." + getFormat());
	        OutputStreamWriter writerOutputStream = new OutputStreamWriter(
	        		new BufferedOutputStream(
	        				new FileOutputStream(targetFile), 
	        				1024), 
	        		"UTF-8");
	        List<InvoiceEntity> batch = new ArrayList<> (1000);
	        ListIterator<InvoiceEntity> listIterator = entities.listIterator();
	        ObjectWriter csvWriter = mapper.writer(schema);

	        while (listIterator.hasNext()) {
	        	InvoiceEntity entity = listIterator.next();
		        entity.setInvoiceImage(correlationService.outboundCorrelation(entity.getImageName()));

	        	batch.add(entity);
	        	listIterator.remove();
	        	if (batch.size() % 1000 == 0) {
	    	        csvWriter.writeValue(writerOutputStream, batch);
	    	        writerOutputStream = new OutputStreamWriter(
	    	        		new BufferedOutputStream(
	    	        				new FileOutputStream(
	    	        						new File(baseOututLocation(), customer + "-" + suffix + "." + getFormat()),
	    	        						true), 
	    	        				1024), 
	    	        		"UTF-8");
	    	        batch.clear();

	        	}
	        }
	        if (batch.size() > 0) {
	        	csvWriter.writeValue(writerOutputStream, batch);
	        }
	        LOGGER.info(size + " invoices written to: " + targetFile.getAbsolutePath());
	        return size;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return -1;
		}
	}

	@Override
	public String baseOututLocation() {
		return baseLocation;
	}

	@Override
	public String getFormat() {
		return "csv";
	}
}
