package com.taulia.converter.processor.outbound;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.taulia.converter.entity.InvoiceEntity;

public class XmlOutboundProcessor implements OutboundProcessor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(XmlOutboundProcessor.class);
	private final String baseLocation;
	private final XmlMapper mapper;
	
	public XmlOutboundProcessor(String baseLocation, XmlMapper mapper) {
		this.baseLocation = baseLocation;
		this.mapper = mapper;
 	}

	public int process(String customer, List<InvoiceEntity> entities) {
		try {
			String correlationId = UUID.randomUUID().toString();
			mapper.writer().withRootName("invoices").writeValue(new File(baseOututLocation(), customer + "-" + correlationId + "." + getFormat()), entities);
			return entities.size();
		} catch (IOException e)   {
			LOGGER.error(e.getMessage(), e);
			return -1;
		}
	}



	@Override
	public String baseOututLocation() {
		return baseLocation;
	}

	@Override
	public String getFormat() {
		return "xml";
	}
}
