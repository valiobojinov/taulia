package com.taulia.converter.processor.outbound;

import java.util.List;

import com.taulia.converter.entity.InvoiceEntity;


public interface OutboundProcessor {
	String baseOututLocation();
	String getFormat();
	int process(String customer, List<InvoiceEntity> entities);
}
