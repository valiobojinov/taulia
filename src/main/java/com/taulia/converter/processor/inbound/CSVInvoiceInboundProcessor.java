package com.taulia.converter.processor.inbound;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import com.taulia.converter.entity.InvoiceEntity;
import com.taulia.converter.entity.InvoiceEntityFactory;
import com.taulia.converter.entity.events.CustomerInvoicesParsedEvent;
import com.taulia.converter.services.correlation.PayloadCorrelationService;

/**
 * 
 * @author valentin.bojinov@yahoo.com
 *
 */
@Component
public class CSVInvoiceInboundProcessor implements ApplicationEventPublisherAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(CSVInvoiceInboundProcessor.class);
	private int maxRetries = 10;
	
	@Value("${invoice.parser.input.threads}")
	private int threads = 2;
	

	@Autowired
	private PayloadCorrelationService correlationService;

	@Autowired
	private InvoiceEntityFactory factory;

	private ApplicationEventPublisher eventPublisher;

	private ExecutorService executorService = Executors.newFixedThreadPool(threads);

	public int process(String file) throws IOException, InterruptedException, TimeoutException {
		LOGGER.info("Starting inbound processing of {} using {} threads", file, threads);
		Queue<InvoiceEntity> invoicesList = new ConcurrentLinkedDeque<>();
		Set<String> buyersSet = ConcurrentHashMap.newKeySet();

		long startTime = System.currentTimeMillis();
		
		LineIterator lineIterator = FileUtils.lineIterator(new File(file));
		lineIterator.next();
		while (lineIterator.hasNext()) {
			String line = lineIterator.nextLine();
			executorService.submit(new LineConsumerRunnableTask(invoicesList, buyersSet, line, factory, correlationService));
			line = null;
		}
		lineIterator.close();
		
		executorService.shutdown();
		boolean terminated = false;

		int retryAttempts = 0;
		while (!terminated && retryAttempts < maxRetries) {
			terminated = executorService.awaitTermination(100, TimeUnit.MILLISECONDS);
		}
		if (!terminated) {
			throw new TimeoutException();
		}
		aggregate(buyersSet, invoicesList);

		LOGGER.info("Processed {} invoices in {} ms", invoicesList.size(), (System.currentTimeMillis() - startTime));
		return invoicesList.size();

	}
	

	private void aggregate(Set<String> buyersSet, Queue<InvoiceEntity> invoicesList) {
		for (String buyer : buyersSet) {
			List<InvoiceEntity> invoicesForBuyer = invoicesList.stream()
					.filter(entity -> buyer.equalsIgnoreCase(entity.getBuyer())).collect(Collectors.toList());
			LOGGER.info("Aggregated {} invoices for buyer {}", invoicesForBuyer.size(), buyer);
			eventPublisher.publishEvent(new CustomerInvoicesParsedEvent(invoicesForBuyer, buyer));
		}
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	private static class LineConsumerRunnableTask implements Runnable {
		private String line;
		private final Queue<InvoiceEntity> invoiceList;
		private final Set<String> buyersSet;
		private final InvoiceEntityFactory factory;
		private final PayloadCorrelationService correlationService;



		public LineConsumerRunnableTask(Queue<InvoiceEntity> invoiceList, Set<String> buyersSet, String line, InvoiceEntityFactory factory, PayloadCorrelationService correlationService) {
			this.invoiceList = invoiceList;
			this.buyersSet = buyersSet;
			this.line = line;
			this.correlationService = correlationService;
			this.factory = factory;
		}
		
		@Override
		public void run() {
			InvoiceEntity invoice = factory.fromCsvLine(line);
			invoice.setInvoiceImage(correlationService.inboundCorrelation(invoice).getBytes());
			invoiceList.add(invoice);
			buyersSet.add(invoice.getBuyer());
			line = null;
		}
	}

}
