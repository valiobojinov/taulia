package com.taulia.converter.entity.events;

import java.util.List;

import org.springframework.context.ApplicationEvent;

import com.taulia.converter.entity.InvoiceEntity;

public class CustomerInvoicesParsedEvent extends ApplicationEvent {
    private final String customerName;
    
	public CustomerInvoicesParsedEvent(List<InvoiceEntity> source, String customerName) {
		super(source);
		this.customerName = customerName;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceEntity> getSource() {
		return (List<InvoiceEntity>) super.getSource();
	}
	
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1357491920705681116L;

}
