package com.taulia.converter.entity.events.listener;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.taulia.converter.entity.InvoiceEntity;
import com.taulia.converter.entity.events.CustomerInvoicesParsedEvent;
import com.taulia.converter.processor.outbound.XmlOutboundProcessor;

@Order(5)
@Component
public class XmlInvoiceEntityParsedEventListener implements ApplicationListener<CustomerInvoicesParsedEvent> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(XmlInvoiceEntityParsedEventListener.class);
	
	/*
	 * * Counter mainly used for testing purposes
	 */
	private AtomicInteger parsedEntitesCount;
	
	@Value("${target.directory}")
	private String baseLocation;
	
	private XmlOutboundProcessor processor; 
	
	
	@PostConstruct
	public void init() {
 		parsedEntitesCount = new AtomicInteger(0);
		XmlMapper mapper = new XmlMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
		mapper.addMixIn(InvoiceEntity.class, MixIn.class);
		processor = new XmlOutboundProcessor(baseLocation, mapper);
	}
	
	public void reinitialize(String location) {
 		parsedEntitesCount = new AtomicInteger(0);
		XmlMapper mapper = new XmlMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
		mapper.addMixIn(InvoiceEntity.class, MixIn.class);
		processor = new XmlOutboundProcessor(location, mapper);
	}
	
	@Override
	public void onApplicationEvent(CustomerInvoicesParsedEvent event) {
		try {
			int processed = processor.process(event.getCustomerName(), event.getSource());
			parsedEntitesCount.addAndGet(processed);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}



	public int getParsedEntitesCount() {

		return parsedEntitesCount.intValue();
	}
	
	private static abstract class MixIn {
		  @JsonIgnore abstract String getInvoiceImage(); // we don't need it
	}

}
