package com.taulia.converter.entity.events.listener;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.taulia.converter.entity.events.CustomerInvoicesParsedEvent;
import com.taulia.converter.processor.outbound.CsvOutboundProcessor;
import com.taulia.converter.services.correlation.PayloadCorrelationService;

@Order(10)
@Component
public class CsvInvoiceEntityParsedEventListener implements ApplicationListener<CustomerInvoicesParsedEvent> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CsvInvoiceEntityParsedEventListener.class);
			/*
	 * * Counter mainly used for testing purposes
	 */
	private AtomicInteger parsedEntitesCount;
	
	@Value("${target.directory}")
	private String baseLocation;

	private CsvOutboundProcessor processor; 

	
	@Autowired
	private PayloadCorrelationService correlationService;
	
	
	@PostConstruct
	public void init() {
		parsedEntitesCount = new AtomicInteger(0);
		
		CsvMapper mapper = new CsvMapper();
		processor = new CsvOutboundProcessor(baseLocation, mapper, correlationService);

	}
	
	public void reinitialize(String location) {
 		parsedEntitesCount = new AtomicInteger(0);
		CsvMapper mapper = new CsvMapper();
		correlationService.reinitialize(location);
		processor = new CsvOutboundProcessor(location, mapper, correlationService);
	}
	
	
	@Override
	public void onApplicationEvent(CustomerInvoicesParsedEvent event) {
		try {
			int processed = processor.process(event.getCustomerName(), event.getSource());
			parsedEntitesCount.addAndGet(processed);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	public int getParsedEntitesCount() {

		return parsedEntitesCount.intValue();
	}

}
