package com.taulia.converter.entity.events.listener;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractEntiyParsedEventListener<M extends ObjectMapper> {
	protected abstract M getObjectMapper();
	protected abstract boolean isEnabled();
	
}
