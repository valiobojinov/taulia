package com.taulia.converter.entity;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class InvoiceEntityFactory {
	private static final String COLUMN_BUYER = "buyer";
	private static final String COLUMN_IMAGE_NAME = "image_name";
	private static final String COLUMN_INVOICE_IMAGE = "invoice_image";
	private static final String COLUMN_INVOICE_DUE_DATE = "invoice_due_date";
	private static final String COLUMN_INVOICE_NUMBER = "invoice_number";
	private static final String COLUMN_INVOICE_AMOUNT = "invoice_amount";
	private static final String COLUMN_INVOICE_CURRENCY = "invoice_currency";
	private static final String COLUMN_INVOICE_STATUS = "invoice_status";
	private static final String COLUMN_SUPPLIER = "supplier";

	private static final List<String> COLUMN_KEYS = Collections.unmodifiableList(Arrays.<String>asList(
			COLUMN_BUYER,COLUMN_IMAGE_NAME,COLUMN_INVOICE_IMAGE,COLUMN_INVOICE_DUE_DATE, COLUMN_INVOICE_NUMBER,
			COLUMN_INVOICE_AMOUNT,COLUMN_INVOICE_CURRENCY,COLUMN_INVOICE_STATUS,COLUMN_SUPPLIER));
	
	public static final Set<String> CORRELATED_COLUMNS_KEYS = Collections.unmodifiableSet(new HashSet<String>( Arrays.<String>asList(COLUMN_INVOICE_IMAGE)));

	//@Autowired
	//private CorrelationService correlation;
	
	public InvoiceEntity fromCsvLine(String line) {
		int startIndex = 0;
		int separatorIndex = line.indexOf(',');
		
		Map<String, String> invoiceMap = new HashMap<String, String>();
		for (int position = 0; ; position++, startIndex = separatorIndex+ 1, separatorIndex = line.indexOf(',', startIndex)) {
				String value = line.substring(startIndex, (separatorIndex > 0) ? separatorIndex : line.length()-1);
				invoiceMap.put(COLUMN_KEYS.get(position), value);
				if (separatorIndex ==-1) break;
		}
		
		//	public InvoiceEntity( String supplier, String imageName) {
		Double amount = null;
		try {
			amount = Double.valueOf(invoiceMap.get(COLUMN_INVOICE_AMOUNT));
		} catch (NumberFormatException e) {
			//XXX log error and leave it blank
		}

		return new InvoiceEntity(invoiceMap.get(COLUMN_BUYER), 
				invoiceMap.get(COLUMN_INVOICE_DUE_DATE), 
				invoiceMap.get(COLUMN_INVOICE_NUMBER), 
				invoiceMap.get(COLUMN_INVOICE_CURRENCY), 
				invoiceMap.get(COLUMN_INVOICE_STATUS), 
				amount,
				invoiceMap.get(COLUMN_SUPPLIER), 
				invoiceMap.get(COLUMN_IMAGE_NAME),
				invoiceMap.get(COLUMN_INVOICE_IMAGE).getBytes());
				
	}
}
