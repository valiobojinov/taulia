package com.taulia.converter.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"buyer", "image_name","invoice_image", "invoice_due_date", "invoice_number", "invoice_amount", "invoice_currency", "invoice_status", "supplier" })
public class InvoiceEntity {
	//﻿buyer,image_name,invoice_image,invoice_due_date,invoice_number,invoice_amount,invoice_currency,invoice_status,supplier

	private final String buyer;
	private final String dueDate;
	private final String invoiceNumber;
	private final Double invoiceAmount;
	private final String invoiceCurrency;
	private final String invoiceStatus;
	private final String supplier;

	private String imageName;
	private byte[] invoiceImage;
	

	public InvoiceEntity(String buyer, String dueDate, String invoiceNumber, String invoiceCurrency, String invoiceStatus, Double invoiceAmount, String supplier, String imageName, byte[] invoiceImage) {
		this.buyer = buyer;
		this.dueDate = dueDate;
		this.invoiceImage = invoiceImage;
		this.invoiceNumber = invoiceNumber;
		this.invoiceCurrency = invoiceCurrency;
		this.invoiceStatus = invoiceStatus;
		this.invoiceAmount = invoiceAmount;
		this.supplier = supplier;
		this.imageName = imageName;
	}

	@JsonProperty("buyer")
	public String getBuyer() {
		return buyer;
	}
	
	@JsonProperty("invoice_due_date")
	public String getDueDate() {
		return dueDate;
	}
	
	@JsonProperty("image_name")
	public String getImageName() {
		return imageName;
	}
	
	@JsonProperty("invoice_number")
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	@JsonProperty("invoice_amount")
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	@JsonProperty("invoice_currency")
	public String getInvoiceCurrency() {
		return invoiceCurrency;
	}
	@JsonProperty("invoice_status")
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	@JsonProperty("supplier")
	public String getSupplier() {
		return supplier;
	}

	@JsonProperty("invoice_image")
	public byte[] getInvoiceImage() {
		return invoiceImage;
	}
	
	public void setInvoiceImage(byte[] invoiceImage) {
		this.invoiceImage = invoiceImage;
	}
	
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
