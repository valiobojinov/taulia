package com.taulia.converter.app.task;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import com.taulia.converter.entity.InvoiceEntity;
import com.taulia.converter.entity.InvoiceEntityFactory;

public class LineConsumerRunnableTask implements Runnable {
	private final String line;
	private final CopyOnWriteArrayList<InvoiceEntity> invoiceList;
	private final Set<String> buyersSet;
	private final InvoiceEntityFactory factory;

	public LineConsumerRunnableTask(CopyOnWriteArrayList<InvoiceEntity> invoiceList, Set<String> buyersSet,
			 String line, InvoiceEntityFactory factory) {
		this.line = line;
		this.invoiceList = invoiceList;
		this.buyersSet = buyersSet;
		this.factory = factory;
	}

	@Override
	public void run() {
		InvoiceEntity invoice = factory.fromCsvLine(line);
		
		invoiceList.add(invoice);
		buyersSet.add(invoice.getBuyer());
	}
}