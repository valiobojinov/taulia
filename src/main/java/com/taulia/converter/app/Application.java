package com.taulia.converter.app;

import java.io.File;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.taulia.converter.processor.inbound.CSVInvoiceInboundProcessor;
/**
 * The idea behind the application is inspired by apache camel integration engine but is using plain java streams
 * It is a spring boot application that has an inbound and outbound processor a correlation service and uses async tasks to boost performance
 * 
 * @author valentin.bojinov@yahoo.com
 *
 */
@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = { "com.taulia.converter.processor, ", "com.taulia.converter.entity", "com.taulia.converter.services.correlation" })
public class Application implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory
  	      .getLogger(Application.class);
    
    @Autowired
    private CSVInvoiceInboundProcessor processor;
    

	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		SimpleCommandLinePropertySource propertySource = new SimpleCommandLinePropertySource(args);
		
		String threads = propertySource.getProperty("invoice.parser.input.threads");
		String input = propertySource.getProperty("input");

		String targetPath = propertySource.getProperty("target.directory");
		LOGGER.info("Convertor started with {} worker threads and source path {} and output path {}", threads, input, targetPath);
		Files.createDirectories(new File(targetPath, "images").toPath());
		
		processor.process(input);
	}

}
