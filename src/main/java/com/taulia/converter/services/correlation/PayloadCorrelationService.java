package com.taulia.converter.services.correlation;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Base64;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.taulia.converter.entity.InvoiceEntity;

@Service
public class PayloadCorrelationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PayloadCorrelationService.class);

	@Value("${target.directory}")
	private String targetDirectory;

	public String inboundCorrelation(InvoiceEntity entity)  {
		String correlationId = UUID.randomUUID().toString();

		String filename = entity.getBuyer() + "_" + entity.getInvoiceNumber() + "_" + correlationId + "_"
				+ entity.getImageName();
		
		File imageFile = new File(targetDirectory + "/images", filename);
		entity.setImageName(filename);

		try {
			OutputStream stream = Files.newOutputStream(imageFile.toPath(), StandardOpenOption.CREATE_NEW);
			
			stream.write(Base64.getDecoder().decode(entity.getInvoiceImage()));
			stream.close();
		} catch (Exception e) {
			LOGGER.error("Failed to decode image for " + filename, e);
		}
		return correlationId;

	}
	
	public void reinitialize(String location) {
		this.targetDirectory = location;
	}
	
	public byte[] outboundCorrelation(String filename) {
		File imageFile = new File(targetDirectory + "/images", filename);
		try {
			return Files.readAllBytes(imageFile.toPath());
		} catch (IOException e) {
			LOGGER.error("Failed to encode image for " + filename, e);
		}
		return new byte[] {};
	}
}
