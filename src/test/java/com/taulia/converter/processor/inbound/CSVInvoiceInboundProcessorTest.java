package com.taulia.converter.processor.inbound;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.taulia.converter.entity.InvoiceEntityFactory;
import com.taulia.converter.entity.events.listener.CsvInvoiceEntityParsedEventListener;
import com.taulia.converter.entity.events.listener.XmlInvoiceEntityParsedEventListener;
import com.taulia.converter.services.correlation.PayloadCorrelationService;

@ContextConfiguration(classes = { CSVInvoiceInboundProcessor.class, InvoiceEntityFactory.class,
		XmlInvoiceEntityParsedEventListener.class, CsvInvoiceEntityParsedEventListener.class, PayloadCorrelationService.class })
@SpringBootTest(properties = { "target.directory=test" })
public class CSVInvoiceInboundProcessorTest {

	
	@Autowired
	private CSVInvoiceInboundProcessor instance;
	
	@Autowired
	private XmlInvoiceEntityParsedEventListener xmlListener;
	
	@Autowired
	private CsvInvoiceEntityParsedEventListener csvListener;
	
	@BeforeAll
	public static void prepare() throws IOException {
		File source = new File(CSVInvoiceInboundProcessorTest.class.getClassLoader().getResource("invoices.csv").getPath());
		
		BufferedReader reader = Files.newBufferedReader(source.toPath());

		String headerLine = reader.lines().findFirst().get();
		List<String> csvLines = new ArrayList<>();
		reader.lines().forEach((String line) -> {
			csvLines.add(line);
		});
		BufferedWriter writer = Files.newBufferedWriter(new File(source.getParent(), "invoices-big-data.csv").toPath(),
				StandardOpenOption.CREATE);
		writer.write(headerLine);
		int factor = 2000;
		for (int i = 0; i < factor; i++) {
			csvLines.forEach((String line) -> {
				try {
					writer.newLine();
					writer.write(line);
				} catch (IOException e) {
					//XXX just ignore this one it is a test fixture after all
				}

			});
		}
		writer.newLine();
		writer.close();
		
	}
	
	@BeforeEach
	public void setup() {
		xmlListener.reinitialize(getClass().getClassLoader().getResource(".").getPath()+ "data");
		csvListener.reinitialize(getClass().getClassLoader().getResource(".").getPath()+ "data");
		
	}
	
	@Test
	public void processTest() throws InterruptedException, IOException, TimeoutException {
		assertEquals(20000,
				instance.process(getClass().getClassLoader().getResource("invoices-big-data.csv").getPath()));
		assertEquals(20000, xmlListener.getParsedEntitesCount());
		assertEquals(20000, csvListener.getParsedEntitesCount());
	}
}
