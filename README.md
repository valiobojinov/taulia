# Task

At Taulia we often deal with ingesting data and converting it into target formats for consumption into different systems - source and destination systems vary from accounting systems, data-extracts to REST APIs or SOAP calls. An example file is attached and contains invoice data (amounts, identifiers etc) as well as a base64 encoded invoice image. The real files can be 2GB+.**

**Problem description:  
We need to write a system that parses and ingests the given (large) file and has the ability to produce the two different output formats specified below.  
As a user of that system I need to be able to configure or otherwise specify which of the two output formats should be produced.  
The new output formats will then later on be ingested by other systems - the integrity of data and files has to stay. The later ingestion of the newly produced files is not part of this exercise.** 

**The two destination formats should be:  
1. CSV file of the original data but split up by 'buyer'. So if there are 10 different buyers overall there should be 10 different output files.  The rest of the data in the CSV should be arranged in the same way as in the input file.  
2. XML file of the original data split up by 'buyer'. The invoice image should not be part of the XML data but the single invoice files should be extracted from the CSV and be placed into the file-system. The format of the XML should loosely follow the input CSV in regards to node-names etc.****You can decide any changes to folder-structure etc. of the output format.  
It is up to you what language you develop the solution in as long as we can see the solution running and walk through the code and output files you produced together with.**

**Unit tests would be appreciated.**


# Solution
I have implemented a Spring Boot based solution, inspired by Apache Camel 
The input data is parsed by a multithreaded component **CSVInvoiceInboundProcessor** 
The number of the worker threads is configurable via a spring boot property ***invoice.parser.input.threads***- the processor uses threadsafe collections and java streaming API to Spring application events to provide the data to the Outbound processors - those are the components that produce data in the required data formats currently - xml /**XmlOutboundProcessor**/ and csv /**CsvOutboundProcessor**, however, they implement a common interface, that enables further extentions, e.g. Json implemention could easily be added as the core of the outbound processors is based on Jackson. The outbound processors are serial and produce the output in batches.
A correlation service - **PayloadCorrelationService** handles consistency between the detached payloads and the produced files.
For production purposes this correlation service could be easily replaced by a real-life message broker, like Kafka for instance.
s an E2E unit test that generates 2GB bits of data and tests is processing.
It processes the 2GB file with 2 inbound worker threads for about a minute. 
Here's sample output of the processing log:
2020-06-02 11:47:56.193  INFO 23705 --- [           main] c.t.c.p.i.CSVInvoiceInboundProcessor     : Starting inbound processing of /home/valio/git/taulia/target/test-classes/invoices-big-data.csv using 2 threads
2020-06-02 11:48:21.873  INFO 23705 --- [           main] c.t.c.p.i.CSVInvoiceInboundProcessor     : Aggregated 6000 invoices for buyer Traksas
2020-06-02 11:48:34.613  INFO 23705 --- [           main] c.t.c.p.outbound.CsvOutboundProcessor    : 6000 invoices written to: /home/valio/git/taulia/target/test-classes/data/Traksas-5024cbc8-f4d6-4ad6-9a31-5b81182f45ac.csv
2020-06-02 11:48:34.617  INFO 23705 --- [           main] c.t.c.p.i.CSVInvoiceInboundProcessor     : Aggregated 12000 invoices for buyer South African Gold Mines Corp
2020-06-02 11:48:57.758  INFO 23705 --- [           main] c.t.c.p.outbound.CsvOutboundProcessor    : 12000 invoices written to: /home/valio/git/taulia/target/test-classes/data/South African Gold Mines Corp-a23d0c16-c120-4b67-9a99-8813325f46f2.csv
2020-06-02 11:48:57.761  INFO 23705 --- [           main] c.t.c.p.i.CSVInvoiceInboundProcessor     : Aggregated 2000 invoices for buyer Axtronics
2020-06-02 11:49:01.585  INFO 23705 --- [           main] c.t.c.p.outbound.CsvOutboundProcessor    : 2000 invoices written to: /home/valio/git/taulia/target/test-classes/data/Axtronics-2b648223-ffd4-4f76-b7e0-423daffc5847.csv
2020-06-02 11:49:01.586  INFO 23705 --- [           main] c.t.c.p.i.CSVInvoiceInboundProcessor     : Processed 20000 invoices in 65392 ms


Test coverage is 87% 
The unit tests store the processed data in target/test-classes/data and intentionally don't clean it up, so that it can be reviewed more easily.
To run the project locally just build it the unit test will generate a 2GB file that can be reused, should you decide to try increasing the load, just have a look at
CSVInvoiceInboundProcessorTest#prepare it reads the invoices.csv and dumps it into the invoices-big-data.csv file currently 2000 times, feel free to increase this one, if you want
mvn spring-boot:run -Dspring-boot.run.arguments="--invoice.parser.input.threads=3 --target.directory=/home/valio/data/invoices/ --input=/home/valio/git/taulia/target/test-classes/invoices-big-data.csv"

